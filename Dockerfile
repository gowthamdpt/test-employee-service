FROM openjdk:8-jdk-alpine

WORKDIR /
RUN apk add ca-certificates wget && update-ca-certificates
RUN wget -q -O nr.zip https://repo1.maven.org/maven2/com/newrelic/agent/java/newrelic-java/5.0.0/newrelic-java-5.0.0.zip && \
    unzip nr.zip && rm -f nr.zip
RUN apk --update add fontconfig ttf-dejavu
VOLUME /newrelic
ENV NEW_RELIC_APP_NAME employee-service
ENV NEW_RELIC_LICENSE_KEY 2692fda2fae581dd49df5b6735e98bd6083ac818

ENV TZ=America/New_York
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone


ARG version="0.0.1"
COPY target/employee-service-${version}-SNAPSHOT.jar /app/employee-service.jar
COPY src/main/resources/* /user/src/app/


EXPOSE 8000
ENTRYPOINT ["java", "-javaagent:/newrelic/newrelic.jar","-Dnewrelic.config.app_name=employee-service-docker", "-jar", "/app/employee-service.jar"]
CMD []
