pipeline {
    agent any

    tools {
        maven 'Maven'
        jdk 'Java'
    }

    environment {
        AWS_REGION = 'ap-south-1'
        S3_BUCKET = 'testbucketgowtham'
        EC2_INSTANCE_ID = 'i-0cb6b0fd1a7c3656a'
    }

    stages {
        stage('Build') {
            steps {
                sh 'mvn clean package -DskipTests'
            }
        }

        stage('Upload to S3') {
            steps {
                withAWS(region: "${AWS_REGION}") {
                    s3Upload(bucket: "${S3_BUCKET}", path: 'my-app/', file: 'target/my-app.jar', acl: 'Private')
                }
            }
        }

        stage('Deploy to EC2') {
            steps {
                script {
                    def artifactUrl = "https://${S3_BUCKET}.s3.${AWS_REGION}.amazonaws.com/my-app/my-app.jar"

                    sshagent(credentials: ['your-ssh-credentials-id']) {
                        sh """
                            ssh -o StrictHostKeyChecking=no ec2-user@${EC2_INSTANCE_ID} '
                                wget -O my-app.jar "${artifactUrl}"
                                sudo systemctl stop my-app
                                nohup java -jar my-app.jar > app.log 2>&1 &
                                sudo systemctl start my-app
                            '
                        """
                    }
                }
            }
        }
    }

    post {
        always {
            deleteDir()
        }
    }
}
