package com.test.employee.controller;

import com.test.employee.model.Employee;
import com.test.employee.ws.api.vo.APIResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.test.employee.service.EmployeeService;
import com.test.employee.ws.api.vo.EmployeesResponse;

//Test comment

@RestController
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    // Get all employees

    @RequestMapping(value = "/v1/employees", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<EmployeesResponse> getAllEmployees(@RequestParam(required = false) String sortBy) {

        EmployeesResponse empResponse = new EmployeesResponse();

        empResponse.setEmployees(employeeService.getEmployees(sortBy == null ? "id" : sortBy));

        ResponseEntity<EmployeesResponse> response = new ResponseEntity<EmployeesResponse>(empResponse, HttpStatus.OK);

        return response;
    }

    // Get employee by ID
    @RequestMapping(value = "/v1/employee/{id}", method = RequestMethod.GET)
    public ResponseEntity<Employee> getEmployeeByID(@PathVariable int id) {

        Employee emp = employeeService.getEmployeeByID(id);

        ResponseEntity<Employee> response = new ResponseEntity<Employee>(emp, HttpStatus.OK);

        return response;
    }

    // Add new employee
    @RequestMapping(value = "/v1/employee", method = RequestMethod.POST)
    public ResponseEntity<APIResponse> addNewEmployee(@RequestBody Employee emp) {

        boolean opResult = employeeService.addNewEmployee(emp);

        APIResponse<String> responseBody = new APIResponse<String>();
        String responseMsg = opResult ? "Successfully new employee added" : "Failed to add new employee added";
        responseBody.setResponse(responseMsg);
        HttpStatus httpStatus = opResult ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR;

        ResponseEntity<APIResponse> response = new ResponseEntity<APIResponse>(responseBody, httpStatus);
        return response;
    }


    // Delete employee

}

