package com.test.employee.ws.api.vo;

import java.util.List;

import com.test.employee.model.Employee;

public class EmployeesResponse {
	
	private List<Employee> employees;

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	@Override
	public String toString() {
		return "GetEmployeesResponse [employees=" + employees + "]";
	}
	
}
