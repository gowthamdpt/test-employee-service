package com.test.employee.service;

import java.util.List;

import com.test.employee.model.Employee;

public interface EmployeeService {

	// getEmployees
	public List<Employee> getEmployees(String sortBy);

	// getEmployeeByID
	public Employee getEmployeeByID(int id);

	// createEmployee
	public boolean addNewEmployee(Employee e);

	// deleteEmployee
	public boolean deleteEmployee(int id);

}
