package com.test.employee.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.test.employee.model.Employee;
import com.test.employee.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	private List<Employee> employees = new ArrayList<Employee>();

	private void printList(List<Employee> emps) {
		// to print any of the implmentation of List
		System.out.println(emps.toString());
	}

	@Override
	public List<Employee> getEmployees(String sortBy) {

		//Collections.sort(employees);

		//Collections.sort(employees, Comparator.comparing(Employee::getName));

		Comparator<Employee> employeeComparator = Comparator.comparing(Employee::getId);

		if(sortBy.equalsIgnoreCase("name")) {
			employeeComparator = Comparator.comparing(Employee::getName);
		}else if(sortBy.equalsIgnoreCase("location")) {
			employeeComparator = Comparator.comparing(Employee::getLocation);
		}else if(sortBy.equalsIgnoreCase("designation")) {
			employeeComparator = Comparator.comparing(Employee::getDesignation);
		}

		Collections.sort(employees, employeeComparator);

		return employees;
	}

	@Override
	public Employee getEmployeeByID(int id) {

//		for(int i=0;i<employees.size();i++) {
//			Employee e = employees.get(i);
//			if(e.getId() == id) {
//				return e;
//			}
//		}

		// using java5 foreach
//		for(Employee e:employees) {
//			if(e.getId() == id) {
//				return e;
//			}
//		}

		// using java8 streams
//		List<Employee> filteredEmp = employees.stream().filter(e -> e.getId() == id).collect(Collectors.toList());

		// using java8 paralell streams
//		List<Employee> filteredEmp = employees.parallelStream().filter(e -> e.getId() == id)
//				.collect(Collectors.toList());

		Employee emp = employees.parallelStream().filter(e -> e.getId() == id).findFirst().orElse(null);


		return emp;

	}

	@Override
	public boolean addNewEmployee(Employee e) {

		employees.add(e);

		return true;
	}

	@Override
	public boolean deleteEmployee(int id) {

		Employee emp = employees.parallelStream().filter(e -> e.getId() == id).findFirst().orElse(null);

		if(emp !=null) {
			employees.remove(emp);
			return true;
		}
		return false;
	}

	@PostConstruct
	public void initializeEmployees() {

		Employee e1 = new Employee(100, "John", "Sanfransico", "Software Engineer");
		employees.add(e1);
		Employee e2 = new Employee(101, "Kale", "NewYork", "Tech Lead");
		employees.add(e2);
		Employee e3 = new Employee(102, "Alice", "NewYork", "QA Engineer");
		employees.add(e3);
		Employee e4 = new Employee(103, "Max", "New Jersy", "Software Engineer");
		employees.add(e4);
		Employee e5 = new Employee(104, "Jennifer", "Sanfransico", "Software Engineer");
		employees.add(e5);

	}

//	public static void main(String a[]) throws JsonProcessingException {
//
//		EmployeeServiceImpl empServiceImpl = new EmployeeServiceImpl();
//		empServiceImpl.initializeEmployees();
//		ObjectMapper mapper = new ObjectMapper();
//		mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
//		System.out.println("All employee:" + mapper.writeValueAsString(empServiceImpl.getEmployees("location")));
//
//		System.out.println("\n\nemployee with ID 103:" + mapper.writeValueAsString(empServiceImpl.getEmployeeByID(103)));
//
//	}

	//Design principles: Open Close Principle (A class is open for extention, closed for modification)

}
